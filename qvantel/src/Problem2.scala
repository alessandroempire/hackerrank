/**
  * Created by alessandro on  29/06/17
  */
object Problem2 {

  def main(args: Array[String]): Unit = {

    val array = Array(1,0)
    val s = solution(array)
    println("solution is " + s)
  }

  /**
    *
    * @param A
    * @return
    */
  def solution(A: Array[Int]): Int = {
    var n: Int = A.length
    var result: Int = 0;
    var i: Int = 0;

    while (i < n -1){
      if (A(i) == A(i+1))
        result = result + 1
      i = i + 1
    }

    var r: Int = -1
    i = 0

    while (i < n) {
      var count: Int = 0
      if (i > 0){
        if (A(i - 1) != A(i))
          count = count + 1
        else
          count = count - 1
      }
      if (i < n - 1){
        if (A(i+1) != A(i))
          count = count + 1
        else
          count = count - 1
      }
      r = Math.max(r, count)
      i = i + 1
    }
    result + r
  }
}
