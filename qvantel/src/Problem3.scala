/**
  * Created by alessandro on  29/06/17
  */
object Problem3 {

  def main(args: Array[String]): Unit = {
    //val array = Array(2,2,3,4,3,3,2,2,1,1,2,5)
    val array = Array(2,2,3,4,3,2,2,1)
    val s = solution(array)
    println("Borders is " + s)
  }

  def solution(a: Array[Int]) = {
    val arraySize = a.length
    arraySize match {
      case 0 | 1 | 2 => 0
      case _ => _solution(a, 0, 0, arraySize-1, 0)
    }
  }

  /**
    * Main method that calculates the number of hills and valleys in the border.
    * @param array The array with the height of the borders
    * @param last  The position of the first repeated element.
    * @param current The current position of the border.
    * @param n The last position of the array
    * @param counter The counter of number of hills and valleys.
    * @return
    */
  def _solution(array: Array[Int], last: Int, current: Int, n:Int, counter: Int): Int = {
    current match {
      case 0 =>
        if (array(counter) == array(current+1))
          _solution(array, current+1, current+1, n, counter)
        else
          _solution(array, current+1, current+1, n, counter+1)
      case `n` => counter+1
      case _ =>
        if (array(counter) == array(current+1))
          _solution(array, last, current+1, n, counter)
        else {
          val hillValley = backwardVerification(array(last-1), array(current))
          if (forwardVerification(hillValley, array(current), array(current+1)))
            _solution(array, current+1, current+1, n, counter+1)
          else
            _solution(array, current+1, current+1, n, counter)
        }
    }
  }

  /**
    * Knowing if the current height is either a hill or valley,
    * verify - accordingly, if it is a hill or valley.
    * @param hillOrValley Whether the backward verification found a hill or valley.
    * @param current The current height.
    * @param lookahead The next immediate height.
    * @return True if it matches all conditions for either hill or valley, false otherwise.
    */
  def forwardVerification(hillOrValley: Int, current: Int, lookahead: Int): Boolean = {
    hillOrValley match {
      case 0 => if (lookahead < current) true else false
      case 1 => if (lookahead > current) true else false
    }
  }

  /**
    * Verify the last inmmediate height to determine if the current
    * height is a possible hill or valley.
    * @param lookbehind the previous immediate height.
    * @param current current height.
    * @return 0 represents it is a hill and 1 represents a valley.
    */
  def backwardVerification(lookbehind: Int, current: Int): Int = {
    if (lookbehind < current)
      0
    else
      1
  }
}
