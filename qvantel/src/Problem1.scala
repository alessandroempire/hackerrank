/**
  * Created by alessandro on  29/06/17
  */
object Problem1 {

  def main(args: Array[String]): Unit = {
    val myList = 1 :: 2 :: 3  :: 10 :: 12 :: 14 :: 1 :: 2 :: 3 :: Nil
    val sum = solution(myList.toArray)
    println("The sum is " + sum)
  }

  /**
    *
    * @param array
    * @return
    */
  def solution(array: Array[Int]): Int = {

    array match {
      case null => 0
      case _ if (array.length == 0)  || (array.length == 1) => 0
      case _ =>
        val listOfSubstractions: List[Int] = substractArray(array.head, array.tail, List())

        val groupSubstractionByIdenticalElements: List[List[Any]] = pack(listOfSubstractions)

        val permutationValues: scala.collection.mutable.HashMap[Int, Int] = scala.collection.mutable.HashMap()
        permutationValues += (0 -> 0)
        permutationValues += (1 -> 0)
        permutationValues += (2 -> 1)

        groupSubstractionByIdenticalElements.foldLeft(0)( (acc, l) => acc + counter(l, permutationValues) )
    }
  }

  /**
    * Method that packs consecutive elements into a sublist.
    * @param list
    * @tparam A
    * @return
    */
  def pack[A](list: List[A]): List[List[A]] = {
    def _pack(res: List[List[A]], rem: List[A]):List[List[A]] = rem match {
      case Nil => res
      case h::tail if (res.isEmpty || res.last.head != h) => _pack(res:::List(List(h)), tail)
      case h::tail => _pack(res.init:::List(res.last:::List(h)), tail)
    }
    _pack(List(),list)
  }

  /**
    * Method that calculate the number of combinations possible given n consecutive elements.
    * @param list
    * @param permutationValues
    * @return
    */
  def counter(list: List[Any], permutationValues: scala.collection.mutable.HashMap[Int, Int]): Int = {
    val listSize = list.size
    permutationValues.getOrElse(listSize, recursiveFormula(listSize, permutationValues) )
  }

  /**
    * Recursive formula that calculates the number of combinations given n consecutive-identical elements.
    * @param n
    * @param permutationValues
    * @return
    */
  def recursiveFormula(n: Int, permutationValues: scala.collection.mutable.HashMap[Int, Int]): Int = {

    n match {
      case 2 => 1
      case i =>
        val pn = permutationValues.getOrElse(n, recursiveFormula(n-1, permutationValues) ) + n - 1
        permutationValues += (n -> pn)
        pn
    }
  }


  /**
    * Method that substract two consecutive elements of an array.
    * @param arrayHead
    * @param array
    * @param accumulator
    * @return
    */
  def substractArray(arrayHead: Int, array: Array[Int], accumulator: List[Int]): List[Int] = {

    array match {
      case null => accumulator
      case arr if arr.length == 1 => (arrayHead - arr.head):: accumulator
      case _ => substractArray(array.head, array.tail, (arrayHead - array.head) :: accumulator)
    }
  }

}
