/**
  * Created by alessandro on  29/06/17
  */
object Problem4 {

  def main(args: Array[String]): Unit = {

    //Example 1
    val m = 3
    val x = 5
    val y = 200
    val a = Array(40, 40, 100, 80, 20)
    val b = Array(3, 3, 2, 2, 3)
    val s = solution(a,b, m, x, y)
    println("The count is  " + s)
  }

  /**
    *
    * @param a Weight of person i
    * @param b Target floor of person i
    * @param m Number of last floor
    * @param x Maximum capacity of people of elevator
    * @param y Weight limit of elevator
    * @return
    */
  def solution(a: Array[Int], b: Array[Int], m: Int, x: Int, y: Int): Int = {

    val stops: scala.collection.mutable.HashSet[Integer] = scala.collection.mutable.HashSet[Integer]()

    _solution(a,b, x, y, x, y, 0, stops)
  }

  /**
    *
    * @param a Weight of person i
    * @param b Target floor of person i
    * @param maxCapacity Max capacity of elevator
    * @param maxWeight   Max weight of elevator
    * @param currentCapacity Current capacity of elevator
    * @param currentWeight   Current weight of elevator
    * @param counter Number of stops counted
    * @param stops  HashSet that has the number of the floors to be done in single trip.
    * @return
    */
  def _solution(a: Array[Int], b: Array[Int], maxCapacity: Int, maxWeight: Int,
                currentCapacity: Int, currentWeight: Int, counter: Int,
                stops: scala.collection.mutable.HashSet[Integer]): Int = {

    //
    a match {
      case Array() => counter + 1 //Deliver last person, return to floor 0.
      case _ =>
        val personsWeight = a.head
        if (currentCapacity > 0 && personsWeight <= currentWeight){
          //Can add the person to the elevator
          if (stops.add(b.head)){
            //Person going to a new floor
            _solution(a.tail, b.tail, maxCapacity, maxWeight, currentCapacity -1, currentWeight - personsWeight,
              counter+1, stops)
          } else {
            //Person going to a floor that other persons in the elevator are already going
            _solution(a.tail, b.tail, maxCapacity, maxWeight, currentCapacity-1, currentWeight - personsWeight,
              counter, stops)
          }
        } else {
          //Cannot add the person to the elevator, deliver and go back to floor 0
          _solution(a, b, maxCapacity, maxWeight, maxCapacity, maxWeight, counter+1,
            scala.collection.mutable.HashSet[Integer]())
        }
    }
  }
}
