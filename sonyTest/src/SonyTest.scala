/**
  * Created by alessandro on 5/31/17.
  */
object SonyTest {

  private val operations = scala.collection.immutable.HashSet[String]("POP", "DUP", "+", "-")

  def solution(s: String): Int = {

    //Split the String
    val splittedString: List[String] = s.split("\\s").toList

    val resultingStack = auxSolution(splittedString, List())
    if (resultingStack.isEmpty)
      -1
    else
      resultingStack.head
  }


  def auxSolution(splittedString: List[String], stack: List[Int]): List[Int] = {
    splittedString match {
      case Nil => stack
      case element :: tail =>
        if (operations.contains(element)){
          element match {
            case "POP" if stack.size >= 1 => auxSolution(tail, stack.tail)
            case "DUP" if stack.size >= 1 => auxSolution(tail, stack.head :: stack)
            case "+" if stack.size >= 2 => auxSolution(tail, stack.take(2).sum :: stack.drop(2) )
            case "-" if stack.size >= 2 =>
              val subs = (stack.take(2).reduce(_ - _))
              if (subs < 0 )
                auxSolution(Nil, -1 :: stack)
              else
                auxSolution(tail, (stack.take(2).reduce(_ - _)) :: stack.drop(2) )
            case _ => auxSolution(Nil, -1 :: stack)
          }
        }
        else
        //contains integer
        auxSolution(tail, element.toInt :: stack)
    }
  }

  def main(args: Array[String]): Unit = {
    var s = "10 DUP + 35 -"
    val i = solution(s)
    println(i)
  }

}
