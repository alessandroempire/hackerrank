import scala.collection.mutable

/**
  * Class that defines a Rose Tree
  * @param topic The topic of the current node.
  * @param father The father tree of the current node.
  * @param children The children of the current node.
  * @tparam T
  */
case class RoseTree[T](topic: String, father: RoseTree[String],children: List[RoseTree[String]])  extends Ordered[RoseTree[T]] {

  /**
    * Override toString method.
    * @return String representation of the RoseTree.
    */
  //    override def toString: Predef.String = "RT(" + topic.toString +" Children={" + children.map(_.toString).mkString(",") + "})"
  override def toString: Predef.String = "RT(" + topic.toString + " Father={" + father.topic + "})"
  //    override def toString = "RT(" + topic.toString + " Questions= {" + questions.toString() +"}  Children={" + children.map(_.toString).mkString(",") + "})"

  /**
    * Method that allows RoseTree to be Ordered
    * @param that Other rose tree to compared to.
    * @return Result of the comparison.
    */
  def compare(that: RoseTree[T]): Int =  (this.topic) compare (that.topic)

  /**
    * Method to do a pretty print of the tree structure.
    * This is just to help debugging if needed.
    */
  def prettyPrint(): Unit = {
    def prettyPrintAux(roseTree: List[RoseTree[String]], tab: java.lang.String): java.lang.String = {
      roseTree.foldLeft(" \n")( {
        case (acc, t) => acc + tab + t.topic + " " +prettyPrintAux(t.children, tab + "\t")
      })
    }
    println(topic.toString + prettyPrintAux(this.children, "\t"))
  }
}


/**
  *
  */
object Solution {

  /**
    * Helper method to print the parents of the nodes.
    * @param roseTree Rose Tree to process
    */
  def myToString(roseTree: RoseTree[String]): Unit = {
    if (roseTree.father == null) {
      println("RT(" + roseTree.topic.toString + " Father={No Father})") + "} Children={" + roseTree.children.map(rt => myToString(rt)) + "})"
    } else {
      println("RT(" + roseTree.topic.toString + " Father={" + roseTree.father.topic + "})") + "} Children={" + roseTree.children.map(rt => myToString(rt)) + "})"
    }
  }


  /**
    * Method that parses the flat tree into a RoseTree
    * @param string String of flat tree, with each node separated by a space.
    * @return Rose tree with topics as nodes and their respective questions.
    */
  def parseFlatTree(string: String, hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]],
                    treeLeafs: mutable.HashSet[RoseTree[String]]): RoseTree[String] = {
    val splitted = string.split("\\s")
    if (splitted.length == 1)
      RoseTree(splitted(0), null, List())
    else
      parseFlatTreeTailRecursive(List(splitted(0)), splitted.toList.drop(1), List(),
        List(RoseTree(splitted(0), null, null)), hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]], treeLeafs)
  }


  /**
    * Method that helps parse the flat tree using tail recursion.
    * @param stackOfFathers A list that simulates a stack of the fathers
    * @param flatTree The flat tree to parse
    * @param stackOfChildren A list that simulates a stack, which contains a list of the children.
    * @return
    */
  def parseFlatTreeTailRecursive(stackOfFathers: List[String], flatTree: List[String],
                                 stackOfChildren: List[List[RoseTree[String]]],
                                 stackOfParents: List[RoseTree[String]],
                                 hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]],
                                 treeLeafs: mutable.HashSet[RoseTree[String]]): RoseTree[String] ={
    flatTree match {
      case ")" :: Nil =>
        //Found last ), return.
        RoseTree(stackOfFathers.head, null,  stackOfChildren.head)
      case ")" :: _ =>
        //Closing
        val currentFather = stackOfFathers.head
        val children = stackOfChildren.head
        val newNode = RoseTree[String](currentFather, stackOfParents(1), children)
        val newChildren = newNode :: stackOfChildren.drop(1).head
        //
        parseFlatTreeTailRecursive(stackOfFathers.drop(1), flatTree.drop(1),  newChildren :: stackOfChildren.drop(2),
          stackOfParents.drop(1), hashOfChildren, treeLeafs)
      //First case
      case "(" :: _ if stackOfChildren.isEmpty =>
        parseFlatTreeTailRecursive(stackOfFathers, flatTree.drop(1), List(List()), stackOfParents, hashOfChildren, treeLeafs)
      //Found new nested structure
      case "(":: _  if stackOfChildren.nonEmpty =>
        val childrenFather = stackOfChildren.head
        val newStackOfChildren = childrenFather.drop(1) :: stackOfChildren.drop(1)
        parseFlatTreeTailRecursive( childrenFather.head.topic :: stackOfFathers, flatTree.drop(1),
          List() :: newStackOfChildren, RoseTree[String](childrenFather.head.topic, stackOfParents.head , null) :: stackOfParents,
          hashOfChildren, treeLeafs)
      case topic :: _ =>
        val newNode = RoseTree[String](topic, stackOfParents.head, List())
        val newHead: List[RoseTree[String]] = newNode :: stackOfChildren.head
        updateHashMap(stackOfFathers.head, topic, hashOfChildren) //Update hashofChildren with all children
        if (flatTree(1) != "(")
          treeLeafs += newNode //Suppose
        parseFlatTreeTailRecursive(stackOfFathers, flatTree.drop(1), newHead :: stackOfChildren.drop(1),
          stackOfParents, hashOfChildren, treeLeafs)
    }
  }


  /**
    * Method that determines the fathers that can proceded to update its father given that it has processes all its
    * children
    * @param treeSet The Set of inmmediate updatable nodes.
    * @param questions The hash of questions.
    * @param acc The Hash Accumulator of questions of each node.
    */
  def breadthUpwardUpdate(treeSet: mutable.HashSet[RoseTree[String]], questions:mutable.HashMap[String, mutable.HashSet[String]],
                           acc: mutable.HashMap[String, mutable.HashSet[String]],
                           hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]]): Unit = {

    val chopedTree: mutable.HashSet[RoseTree[String]] = treeSet.map(rt => LeafsUpdates(rt, questions, acc, hashOfChildren))

    if (chopedTree.nonEmpty) {
      if (chopedTree.head.topic == "")
        breadthUpwardUpdate(chopedTree.drop(1), questions, acc, hashOfChildren)
      else
        breadthUpwardUpdate(chopedTree, questions, acc, hashOfChildren)
    }
  }

  /**
    * Method that updates the node of a tree by updating itself and its father with its questions.
    * @param roseTree The node to update.
    * @param questions The questions hasmap to find the node's questions.
    * @param acc  The Hash Accumulator of questions of each node.
    * @return Returns the father node to process.
    */
  def LeafsUpdates(roseTree: RoseTree[String], questions:mutable.HashMap[String, mutable.HashSet[String]],
                   acc: mutable.HashMap[String, mutable.HashSet[String]],
                   hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]]): RoseTree[String] = {

    //Add current node topics questions to itself
    questions.get(roseTree.topic).getOrElse(mutable.HashSet()).map(q => updateHashMap(roseTree.topic, q, acc))

    //Add current node questions to father, if exists
    if (roseTree.father != null){
      acc.get(roseTree.topic).getOrElse(mutable.HashSet()).map(q => updateHashMap(roseTree.father.topic, q, acc))

      if (hasProcessedAllChildren(roseTree.father.topic, roseTree.topic,hashOfChildren))
        roseTree.father
      else
        RoseTree[String]("", null, List())
    }
    else
      RoseTree[String]("", null, List())
  }


  /**
    * Method that determine if all the children have been processed.
    * @param topic Topic to determine if all its children have been procesed.
    * @param hashOfChildren That hash of children to update.
    * @return Returns whether the topic still has children to be procesed.
    */
  def hasProcessedAllChildren(fatherTopic: String, topic: String,hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]]): Boolean = {
    val child = hashOfChildren.get(fatherTopic)
    child match {
      case None => false
      case Some(tree) =>
        !tree.remove(topic)
        if (tree.isEmpty)
          hashOfChildren.remove(fatherTopic)
        else
          hashOfChildren += (fatherTopic -> tree)
        tree.isEmpty
    }
  }


  /**
    * Method that update a Hashmap with a single element. If it exists, it adds it. If it does not exists,
    * create a new entry.
    * @param topic Key
    * @param element Value
    * @param hashMap Hashmap to update
    */
  def updateHashMap(topic: String, element: String, hashMap: mutable.HashMap[String, mutable.HashSet[String]]): Unit = {
    val value = hashMap.get(topic)
    value match {
      case None => hashMap += (topic -> mutable.HashSet(element))
      case Some(tree) =>
        tree += element
        hashMap += (topic ->  tree )
    }
  }


  /**
    * Method that updates a hasMap of Topics -> Tree of questions of that topic when procesing the questions
    * @param topicQuestion String with the topic and its respective question.
    * @param hashMap The hasmap that contains the Topic and its corresponding tree of questions.
    * @return The updated hashmap.
    */
  def updateTopicQuestionTree(topicQuestion: String, hashMap: mutable.HashMap[String, mutable.HashSet[String]],
                              id: Int): mutable.HashMap[String, mutable.HashSet[String]]= {

    val splitTopicQuestions = topicQuestion.split("\\:", 2)
    val listQuestions = hashMap.get(splitTopicQuestions(0))
    listQuestions match {
      case None => hashMap +=  (splitTopicQuestions(0) ->  mutable.HashSet(splitTopicQuestions(1).trim()+" "+id.toString))
      case Some(tree) =>
        tree.add(splitTopicQuestions(1).trim()+" "+id.toString)
        hashMap += (splitTopicQuestions(0) ->  tree)
    }
  }


  /**
    * Method that calculates for a query the number of times it starts with a certain string
    * @param query The query
    * @param questionsMap The Hash that constains for each topic its subtree of questions
    * @return Returns the count.
    */
  def countQueries(query: String, questionsMap: mutable.HashMap[String, mutable.HashSet[String]]): Int ={
    val splitQuery = query.split("\\s", 2)
    questionsMap.get(splitQuery(0)) match {
      case None => 0
      case Some(tree) => tree.count(s => s.startsWith(splitQuery(1)))
    }
  }


  /**
    * Method that parse the file in a list format.
    * @param fileLines The file in a list format.
    */
  def ontology(fileLines: List[String]): Unit = {

    //Getting the topics
    val numTopics = fileLines(0).toInt
    val topics    = fileLines(1)

    //Variable needed
    val hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]] = mutable.HashMap[String, mutable.HashSet[String]]()
    val treeLeafs: mutable.HashSet[RoseTree[String]] = mutable.HashSet()
    val completeQuestionMap: mutable.HashMap[String, mutable.HashSet[String]] =  mutable.HashMap[String, mutable.HashSet[String]]()

    //Generating the parsed tree
    val parsedTree = parseFlatTree(topics, hashOfChildren, treeLeafs)
    parsedTree.prettyPrint()

    //Splitting questions from queries
    val splittedFileLines = fileLines.splitAt(3+fileLines(2).toInt)
    val questionsList = splittedFileLines._1.drop(3)

    //Create a HashMap with the topics as Hashes and the values are the questions of that topic
    val questionsMap: mutable.HashMap[String, mutable.HashSet[String]] = questionsList.foldLeft( (mutable.HashMap[String, mutable.HashSet[String]](),1) )({
      case (acc, string) =>  (updateTopicQuestionTree(string, acc._1, acc._2), acc._2+1)})._1

    //NEW 30
    breadthUpwardUpdate(treeLeafs, questionsMap, completeQuestionMap, hashOfChildren)
//    println(completeQuestionMap)

    //Final part
//    val queries = splittedFileLines._2.tail
//    queries.map(s => countQueries(s, completeQuestionMap)).foreach(println)
  }


  /**
    * Method to count the running time
    * @param block
    * @tparam R
    * @return
    */
  def time[R](block: => R): R = {
    val t0 = System.currentTimeMillis()
    val result = block    // call-by-name
    val t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0))
    result
  }


  /**
    * Main.
    * @param args
    */
  def main(args: Array[String]): Unit = {

    //    //To read in hackerrank
//    var numTopics = readInt // for integers
//    var topics = readLine
//
//    //Variable needed
//    val hashOfChildren: mutable.HashMap[String, mutable.HashSet[String]] = mutable.HashMap[String, mutable.HashSet[String]]()
//    val treeLeafs: mutable.HashSet[RoseTree[String]] = mutable.HashSet()
//
//    //Generating the parsed tree
//    val parsedTree = parseFlatTree(topics, hashOfChildren, treeLeafs)
//
//    var n1 = readInt
//    var questionsMap = new mutable.HashMap[String, mutable.HashSet[String]]()
//    for (i <- 1 to n1){
//      updateTopicQuestionTree(readLine.toString, questionsMap, i)
//    }
//
//    //Create a HashMap with topic and values its questions plus the questions of its respective subtree.
//    val completeQuestionMap: mutable.HashMap[String, mutable.HashSet[String]] =  mutable.HashMap[String, mutable.HashSet[String]]()
//    breadthUpwardUpdate(treeLeafs, questionsMap, completeQuestionMap, hashOfChildren)
//
//    //Final part
//    var n2 = readInt
//    var queries = List("")
//    for (i <- 1 to n2) {
//      queries =  readLine :: queries
//    }
//    queries.reverse.drop(1).map(s => countQueries(s, completeQuestionMap)).foreach(println)

    val filename = "/home/alessandro/git/hackerrank/ontology/src/data73.txt"
    time {ontology(io.Source.fromFile(filename).getLines.toList)}
  }



}
